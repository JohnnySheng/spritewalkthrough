//
//  SpriteAppDelegate.h
//  SpriteWalkthrough
//
//  Created by Sheng Johnny on 13-8-20.
//  Copyright (c) 2013年 Sheng Yuangang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpriteAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
