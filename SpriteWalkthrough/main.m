//
//  main.m
//  SpriteWalkthrough
//
//  Created by Sheng Johnny on 13-8-20.
//  Copyright (c) 2013年 Sheng Yuangang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SpriteAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SpriteAppDelegate class]));
    }
}
